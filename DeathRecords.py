from pyspark import SparkConf, SparkContext

conf = SparkConf().setMaster("local").setAppName("Death")
sc = SparkContext(conf = conf)

def extract(line):
    fields = line.split(',')
    if int(fields[19]) == 1 :
        death = "Accident"
    if int(fields[19]) == 2 :
        death = "Suicide"
    if int(fields[19]) == 3 :
        death = "Homicide"
    if int(fields[19]) == 4 :
        death = "Pending Investigation"
    if int(fields[19]) == 5 :
        death = "Could not determine"
    if int(fields[19]) == 6 :
        death = "Self inflicted"
    if int(fields[19]) == 7 :
        death = "Natural"
    if int(fields[19]) == 0 :
        death = "Not Specified"
        
    if int(fields[7]) == 0 :
        return ("Infant", death)
    if int(fields[8]) < 13 :
        return("Kid", death)
    if int(fields[8]) < 26 :
        return("Teen", death)
    if int(fields[8]) < 41 :
        return("Adult", death)
    if int(fields[8]) < 61 :
        return("Middle Age", death)
    return("Old",death)    
        

input = sc.textFile("file:///sparkcode/deathrecords/DeathRecords.csv")
mappedInput = input.map(extract)
counted = mappedInput.countByValue()

results = mappedInput.collect()
for result in results:
    print result